package com.example.jacek.lodowka.model;

import java.util.Calendar;

/**
 * Created by Wojtek on 2016-12-05.
 */

public class ProductModel {
    String name;
    Calendar calendar;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Calendar getCalendar() {
        return calendar;
    }

    public void setCalendar(Calendar calendar) {
        this.calendar = calendar;
    }
}
