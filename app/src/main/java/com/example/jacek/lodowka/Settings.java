package com.example.jacek.lodowka;

import android.app.Activity;
import android.content.Context;
import android.preference.PreferenceManager;

/**
 * Created by Wojtek on 2016-12-14.
 */

public class Settings {
    public static int getSettingsTime(Context context) throws Exception {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("days", 0);
    }
    public static void setSettingsTime(Activity activity, int days) throws Exception {
        activity.getPreferences(Context.MODE_PRIVATE).edit().putInt("days", days).apply();
    }
}
