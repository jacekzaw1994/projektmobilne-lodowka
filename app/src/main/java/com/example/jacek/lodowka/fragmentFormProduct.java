package com.example.jacek.lodowka;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.app.Dialog;
import android.app.DatePickerDialog;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import com.example.jacek.lodowka.model.ProductTable;
import com.example.jacek.lodowka.service.SaveInBackgroundService;
import java.text.DateFormat;
import java.util.Calendar;
import static android.content.Context.BIND_AUTO_CREATE;



public class FragmentFormProduct extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    SaveInBackgroundService saveInBackgroundService;
    static int year;
    static int month;
    static int dayOfMonth;
    EditText editText;
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            SaveInBackgroundService.MyBinder myBinder = (SaveInBackgroundService.MyBinder) service;
            saveInBackgroundService = myBinder.getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
        }
    };
    private String mParam1;
    private String mParam2;

    private void setDate(final Calendar calendar, View view) {
        ((Button) view.findViewById(R.id.picDate)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerFragment fragment = new DatePickerFragment();
                fragment.show(getActivity().getSupportFragmentManager(), "date");
            }
        });
        final DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM);
        ((TextView) view.findViewById(R.id.showDate)).setText(dateFormat.format(calendar.getTime()));
    }

    public static class DatePickerFragment extends DialogFragment {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            return new DatePickerDialog(getActivity(),
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            FragmentFormProduct.year = year;
                            FragmentFormProduct.month = month;
                            FragmentFormProduct.year = year;
                        }
                    }, year, month, day);
        }

        /**
         * Create a new instance of DatePickerFragment, providing "num"
         * as an argument.
         */
//        static DatePickerFragment newInstance(int num) {
//            DatePickerFragment f = new DatePickerFragment();
//            return f;
//        }
//
//        @Override
//        public void onCreate(Bundle savedInstanceState) {
//            super.onCreate(savedInstanceState);
//            setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Holo);
//        }
//
//        @Override
//        public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                                 Bundle savedInstanceState) {
//            View v = inflater.inflate(R.layout.fragment_fragment_form_product, container, false);
//            View tv = v.findViewById(R.id.showDate);
//            ((TextView)tv).setText("Date #");
//
////            // Watch for button clicks.
////            Button button = (Button)v.findViewById(R.id.show);
////            button.setOnClickListener(new View.OnClickListener() {
////                public void onClick(View v) {
////                    // When button is clicked, call up to owning activity.
////                    ((FragmentDialog)getActivity()).showDialog();
////                }
////            });
//
//            return v;
//        }

    }

    public FragmentFormProduct() {
        // Required empty public constructor
    }

    public static FragmentFormProduct newInstance(String param1, String param2) {
        FragmentFormProduct fragment = new FragmentFormProduct();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_fragment_form_product, container, false);
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2016);
        calendar.set(Calendar.MONTH, Calendar.DECEMBER);
        calendar.set(Calendar.DAY_OF_MONTH, 10);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 15);
        calendar.set(Calendar.SECOND, 30);
        setDate(calendar, rootView);
        ((Button) rootView.findViewById(R.id.settings)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), SettingsActivity.class));

            }
        });
        final EditText editText = (EditText) rootView.findViewById(R.id.editText);
        ((Button) rootView.findViewById(R.id.button4)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String dateString = String.valueOf(year) + '-' + String.valueOf(month) + '-' + String.valueOf(dayOfMonth);
                ProductTable productTable = new ProductTable();
                productTable.setName(editText.getText().toString());
                productTable.setExpireDate(dateString);
                saveProductInBackground(productTable);
            }
        });

        // Inflate the layout for this fragment
        return rootView;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void saveProductInBackground(ProductTable productTable) {
        Intent boundServiceIntent = new Intent(getContext(), SaveInBackgroundService.class);
        boundServiceIntent.putExtra("Product", productTable);
        getContext().bindService(boundServiceIntent, serviceConnection, BIND_AUTO_CREATE);
    }
}
