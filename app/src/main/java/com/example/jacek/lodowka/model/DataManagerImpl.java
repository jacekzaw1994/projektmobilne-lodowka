package com.example.jacek.lodowka.model;

import android.content.Context;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DataManagerImpl implements DataManager {
    private Context context;

    public DataManagerImpl(Context context) {
        this.context = context;
    }

    @Override
    public ProductModel getProduct(long id) {
        ProductDAO productDAO = new ProductDAO(context);
        ProductTable productTable = productDAO.getProduct(id);
        String expireDate = productTable.getExpireDate();
        SimpleDateFormat format = new SimpleDateFormat("YYYY-MM-DD"); //yyyy-MM-dd'T'HH:mm:ss'Z'
        ProductModel productModel = new ProductModel();
        try {
            Calendar calendar =  toCalendar(format.parse(expireDate));
            productModel.setCalendar(calendar);
            productModel.setName(productTable.getName());
            return productModel;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Calendar toCalendar(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }
}
