package com.example.jacek.lodowka.model;


import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Wojtek on 2016-12-11.
 */

public class ProductTable implements Parcelable {
    long idProduct;
    String name;
    String expireDate;

    public ProductTable() {
        super();
    }

    public ProductTable(long idProduct, String name, String expireDate) {
        this.idProduct = idProduct;
        this.name = name;
        this.expireDate = expireDate;
    }

    public ProductTable(Parcel in) {
        super();
        this.idProduct = in.readLong();
        this.name = in.readString();
        this.expireDate = in.readString();
    }

    public long getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(long idProduct) {
        this.idProduct = idProduct;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    @Override
    public String toString() {
        return name + " " + expireDate;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int)idProduct;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ProductTable other = (ProductTable) obj;
        if (idProduct != other.idProduct)
            return false;
        return true;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeLong(getIdProduct());
        parcel.writeString(getName());
        parcel.writeString(getExpireDate());
    }

    public static final Parcelable.Creator<ProductTable> CREATOR = new Parcelable.Creator<ProductTable>() {
        public ProductTable createFromParcel(Parcel in) {
            return new ProductTable(in);
        }

        public ProductTable[] newArray(int size) {
            return new ProductTable[size];
        }
    };
}
