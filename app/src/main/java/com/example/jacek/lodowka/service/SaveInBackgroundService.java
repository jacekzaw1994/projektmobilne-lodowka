package com.example.jacek.lodowka.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import com.example.jacek.lodowka.model.ProductDAO;
import com.example.jacek.lodowka.model.ProductTable;

public class SaveInBackgroundService extends Service {
    private final IBinder myBinder = new MyBinder();

    public SaveInBackgroundService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        new ProductDAO(getApplicationContext()).save((ProductTable) intent.getParcelableExtra("Product"));
        return myBinder;
    }

    public class MyBinder extends Binder {
        public SaveInBackgroundService getService() {
            return SaveInBackgroundService.this;
        }
    }

}
