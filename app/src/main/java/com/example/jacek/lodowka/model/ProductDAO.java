package com.example.jacek.lodowka.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Wojtek on 2016-12-11.
 */

public class ProductDAO extends ProductTable {
    private SQLiteDatabase database;
    private static final String WHERE_ID_EQUALS = MySQLiteHelper.KEY_PRODUCT_ID
            + " =?";

    public ProductDAO(Context context) {
        MySQLiteHelper mySQLiteHelper = new MySQLiteHelper(context);
        mySQLiteHelper.openWritableMode();
        database = mySQLiteHelper.getDb();
    }

    public long save(ProductTable productTable) {
        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.KEY_PRODUCT_NAME, productTable.getName());
        values.put(MySQLiteHelper.KEY_PRODUCT_EXPIRE_DATE, productTable.getExpireDate());
        Log.d("Create Result: ", productTable.getName());
        return database.insert(MySQLiteHelper.DB_PRODUCT_TABLE, null, values);
    }

    public long save(SQLiteDatabase sqLiteDatabase,ProductTable productTable) {
        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.KEY_PRODUCT_NAME, productTable.getName());
        values.put(MySQLiteHelper.KEY_PRODUCT_EXPIRE_DATE, productTable.getExpireDate());
        Log.d("Create Result: ", productTable.getName());
        return sqLiteDatabase.insert(MySQLiteHelper.DB_PRODUCT_TABLE, null, values);
    }

    public long update(ProductTable productTable) {
        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.KEY_PRODUCT_NAME, productTable.getName());
        values.put(MySQLiteHelper.KEY_PRODUCT_EXPIRE_DATE, productTable.getExpireDate());

        long result = database.update(MySQLiteHelper.DB_PRODUCT_TABLE, values,
                WHERE_ID_EQUALS,
                new String[] { String.valueOf(productTable.getIdProduct()) });
        Log.d("Update Result:", "=" + result);
        return result;

    }

    public int delete(ProductTable productTable) {
        return database.delete(MySQLiteHelper.DB_PRODUCT_TABLE, WHERE_ID_EQUALS,
                new String[] { productTable.getIdProduct() + "" });
    }

    //USING query() method
    public ArrayList<ProductTable> getProducts() {
        ArrayList<ProductTable> productTables = new ArrayList<ProductTable>();

        Cursor cursor = database.query(MySQLiteHelper.DB_PRODUCT_TABLE,
                new String[] { MySQLiteHelper.KEY_PRODUCT_ID,
                        MySQLiteHelper.KEY_PRODUCT_NAME,
                        MySQLiteHelper.KEY_PRODUCT_EXPIRE_DATE }, null, null, null,
                null, null);

        while (cursor.moveToNext()) {
            ProductTable productTable = new ProductTable();
            productTable.setIdProduct(cursor.getLong(0));
            productTable.setName(cursor.getString(1));
            productTable.setExpireDate(cursor.getString(2));

            productTables.add(productTable);
        }
        return productTables;
    }

    public ProductTable getProduct(long id) {
        ProductTable productTable = null;

        String sql = "SELECT * FROM " + MySQLiteHelper.DB_PRODUCT_TABLE
                + " WHERE " + MySQLiteHelper.KEY_PRODUCT_ID + " = ?";

        Cursor cursor = database.rawQuery(sql, new String[] { id + "" });

        if (cursor.moveToNext()) {
            productTable = new ProductTable();
            productTable.setIdProduct(cursor.getLong(0));
            productTable.setName(cursor.getString(1));
            productTable.setExpireDate(cursor.getString(2));
        }
        return productTable;
    }
}
