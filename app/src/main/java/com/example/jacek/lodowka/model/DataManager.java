package com.example.jacek.lodowka.model;

/**
 * Created by Wojtek on 2016-12-12.
 */

public interface DataManager {
    ProductModel getProduct(long id);
}
