package com.example.jacek.lodowka.service;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.Context;
import android.graphics.BitmapFactory;

import com.example.jacek.lodowka.MainActivity;
import com.example.jacek.lodowka.R;
import com.example.jacek.lodowka.Settings;

import java.util.Calendar;

public class ReminderService extends IntentService {

    public ReminderService() {
        super("ReminderService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        
        final NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Calendar calendar =  Calendar.getInstance();
        //TODO FETCH DATE FROM DB
        calendar.set(Calendar.YEAR, 2016);
        calendar.set(Calendar.MONTH, Calendar.DECEMBER);
        calendar.set(Calendar.DAY_OF_MONTH, 10);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 15);
        calendar.set(Calendar.SECOND, 30);
        //calendar.set(date.getYear(), int month, int date, int hour, int minute, int second);
        final long when = calendar.getTimeInMillis();         // notification time
        final Notification notification = new Notification.Builder(getApplicationContext())
                .setContentTitle("ProductTable") //TODO name product
                .setContentText("Your product expires at.") //TODO DATE
                .setSmallIcon(R.drawable.alarm)
                .setLargeIcon(BitmapFactory.decodeResource( getResources(), R.drawable.pobrane))
                .setWhen(when)
                .build();
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent , 0);
        new Thread(new Runnable() {
            public static final int NOTIF_ID = 1;

            @Override
            public void run() {
                while(true) {
                    try {
                        if ((when - Settings.getSettingsTime(getApplicationContext()) * 24 * 3600) - System.currentTimeMillis() < 0) {
                            nm.notify(NOTIF_ID, notification);
                            break;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

    }

}
