package com.example.jacek.lodowka.model;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Wojtek on 2016-12-11.
 */

public class MySQLiteHelper {
    private static final String DEBUG_TAG = "MySQLiteHelper";

    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "fridge.db";
    //tables
    public static final String DB_PRODUCT_TABLE = "product";
    //columns product
    public static final String KEY_PRODUCT_ID = "id_product";
    public static final String PRODUCT_ID_OPTIONS = "INTEGER PRIMARY KEY AUTOINCREMENT";
    public static final int PRODUCT_ID_COLUMN = 0;
    public static final String KEY_PRODUCT_NAME = "name";
    public static final String PRODUCT_NAME_OPTIONS = "TEXT NOT NULL";
    public static final int PRODUCT_NAME_COLUMN = 1;
    public static final String KEY_PRODUCT_EXPIRE_DATE = "expire_date";
    //TEXT as ISO8601 strings ("YYYY-MM-DD HH:MM:SS.SSS").
    public static final String PRODUCT_EXPIRE_DATE_OPTIONS = "TEXT NOT NULL";
    public static final int PRODUCT_EXPIRE_DATE_COLUMN = 2;

    //SQL queries
    private static final String DB_CREATE_PRODUCT_TABLE =
            "CREATE TABLE " + DB_PRODUCT_TABLE + "( " +
                    KEY_PRODUCT_ID + " " + PRODUCT_ID_OPTIONS + ", " +
                    KEY_PRODUCT_NAME + " " + PRODUCT_NAME_OPTIONS + ", " +
                    KEY_PRODUCT_EXPIRE_DATE + " " + PRODUCT_EXPIRE_DATE_OPTIONS + ");";
    private static final String DROP_PRODUCT_TABLE =
            "DROP TABLE IF EXISTS " + DB_PRODUCT_TABLE;
    private SQLiteDatabase db;
    private Context context;
    private DatabaseHelper dbHelper;

    private static class DatabaseHelper extends SQLiteOpenHelper {

        public DatabaseHelper(Context context, String name,
                              SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            Log.d(DEBUG_TAG, "Database creating...");
            db.execSQL(DB_CREATE_PRODUCT_TABLE);
            Log.d(DEBUG_TAG, "Ver." + DB_VERSION + " created");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.d(DEBUG_TAG, "Database updating...");
            db.execSQL(DROP_PRODUCT_TABLE);
            Log.d(DEBUG_TAG, "Updated from ver." + oldVersion + " to ver." + newVersion);
            Log.d(DEBUG_TAG, "Previously version has been removed.");
            onCreate(db);
            Log.d(DEBUG_TAG, "The newest version has been created.");
        }

    }

    public MySQLiteHelper(Context context) {
        this.context = context;
    }

    public MySQLiteHelper openWritableMode(){
        dbHelper = new DatabaseHelper(context, DB_NAME, null, DB_VERSION);
        try {
            db = dbHelper.getWritableDatabase();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return this;
    }

    public MySQLiteHelper openReadableMode(){
        dbHelper = new DatabaseHelper(context, DB_NAME, null, DB_VERSION);
        try {
            db = dbHelper.getReadableDatabase();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return this;
    }

    public void close() {
        dbHelper.close();
    }

    public SQLiteDatabase getDb() {
        return db;
    }

    private String getInsertProductQuery(String date, int position) {
        return "INSERT INTO " + DB_PRODUCT_TABLE + " (name, expire_date) VALUES ('" + KEY_PRODUCT_NAME + position +
                "','" + date + "')";
    }

}
