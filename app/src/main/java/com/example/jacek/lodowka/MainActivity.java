package com.example.jacek.lodowka;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.os.IBinder;
import com.example.jacek.lodowka.model.MySQLiteHelper;
import com.example.jacek.lodowka.model.ProductTable;
import com.example.jacek.lodowka.service.ReminderService;
import com.example.jacek.lodowka.service.SaveInBackgroundService;

public class MainActivity extends FragmentActivity {
    MySQLiteHelper mySQLiteHelper;
    Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startService(new Intent(this, ReminderService.class));
        context = getApplicationContext();
        mySQLiteHelper = new MySQLiteHelper(context);
        mySQLiteHelper.openWritableMode();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment, new FragmentFormProduct()).commit();
        if (findViewById(R.id.fragment) != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }

            // Create a new Fragment to be placed in the activity layout
            FragmentFormProduct firstFragment = FragmentFormProduct.newInstance("Jacu", "bleble");

            // In case this activity was started with special instructions from an
            // Intent, pass the Intent's extras to the fragment as arguments
//            firstFragment.setArguments(getIntent().getExtras());

            // Add the fragment to the 'fragment_container' FrameLayout
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment, firstFragment).commit();
        }
    }

    @Override
    protected void onDestroy() {
        if (mySQLiteHelper != null)
            mySQLiteHelper.close();
        super.onDestroy();
    }

    public void goToProductList(View view) {
        if (findViewById(R.id.fragment) != null) {
            // Create a new Fragment to be placed in the activity layout
            FragmentListProduct firstFragment = FragmentListProduct.newInstance("wojtek", "123");

            // Add the fragment to the 'fragment_container' FrameLayout
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment, firstFragment).commit();
        }
    }

    public void goToAddProductForm(View view) {
        if (findViewById(R.id.fragment) != null) {
            // Create a new Fragment to be placed in the activity layout
            FragmentFormProduct firstFragment = FragmentFormProduct.newInstance("wojtek", "123");

            // Add the fragment to the 'fragment_container' FrameLayout
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment, firstFragment).commit();
        }
    }


}
