package com.example.jacek.lodowka;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class SettingsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
    }

    public void save(View view) {
        String days = ((EditText) findViewById(R.id.editText2)).getText().toString();
        try{
            Settings.setSettingsTime(this, Integer.valueOf(days));
        } catch (NumberFormatException e) {
            Toast.makeText(getApplicationContext(), "Please type an integer", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "An error has occured when trying save settings", Toast.LENGTH_SHORT).show();
        }
    }
}
